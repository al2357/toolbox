import java.util.ArrayList;
import java.util.Scanner;

import math.*;
import sorting.*;

public class Run {

	public static void main(String[] args) {
		String ls = System.getProperty("line.separator");
		
		System.out.println("Select an option or 'q' to quit:");
		System.out.println("1. Merge Sort");
		System.out.println("2. Karatsuba");
		String input = "";
		while(!input.equals("q")) {
			Scanner sc = new Scanner(System.in);
			System.out.print("Your choice: ");
			input = sc.next();
			switch(input) {
				case "1":
					System.out.println("let's merge-sort things out.");
					runMergeSort();
					break;
				case "2":
					System.out.println("karatsuba.");
					runKaratsuba();
					break;
				case "q":
					break;
				default:
					System.out.println("\"do or do not, there is no try\". please do again.");
			}
		}
	}
	
	private static void runMergeSort() {
		ArrayList<Integer> uL = new ArrayList<>();
		uL.add(5);
		uL.add(4);
		uL.add(9);
		uL.add(1);
		uL.add(23);
		uL.add(2);
		uL.add(3);
		uL.add(88);
		uL.add(13);
		uL.add(0);

		MergeSort s = new MergeSort(uL);
		s.printList();
		s.run();
		s.printList();
	}
	
	private static void runKaratsuba() {
		Karatsuba k = new Karatsuba();
		System.out.println(k.multiply(2, 45678)+" / "+2*45678);
		System.out.println(k.multiply(145, 198)+" / "+145*198);
		System.out.println(k.multiply(123, 456)+" / "+123*456);
		System.out.println(k.multiply(1234, 4567)+" / "+1234*4567);
		System.out.println(k.multiply(21, 45678)+" / "+21*45678);
		System.out.println(k.multiply(213, 45678)+" / "+213*45678);
	}

}
