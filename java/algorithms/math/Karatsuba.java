package math;

public class Karatsuba {
	
	/**
	 * Overloading method for multiply.
	 * @param x
	 * @param y
	 * @return
	 */
	public int multiply(int x, int y) {
		return multiply(x, y, true);
	}
	
	/**
	 * Recurring method multiplying two numbers.
	 * @param x
	 * @param y
	 * @param exponentFloor
	 * @return
	 */
	public int multiply(int x, int y, boolean exponentFloor) {
		if(x < 10 || y < 10) {
			return x*y;
		} else {
			// n is maximum amount of digits in either number.
			// if there is number with less digits than 'n' it will be padded with 0s.
			int n = x > y ? String.valueOf(x).length() : String.valueOf(y).length();
			// m is the exponent. 
			// it has to be equal to the second part of the longest number. 
			// floor(n/2): e.g. (000)85*23858; 00(a1)|085(a2) & 23(b1)|858(b2); m = 3;
			// ceil(n/2): e.g. (000)85*23858; 000(a1)|85(a2) & 238(b1)|58(b2); m = 2;
			int m = exponentFloor ? (int)Math.floor((double)n/2.0) : (int)Math.ceil((double)n/2.0);
		
			int[] xArr = splitInTwo(x, m);
			int[] yArr = splitInTwo(y, m);

			int a1 = multiply(xArr[0], yArr[0]);
			int a2 = multiply(xArr[1], yArr[1]);
			int a3 = (xArr[0] + xArr[1])*(yArr[0]+yArr[1])-a1-a2;
			return ((int)Math.pow(10d, (double)2*m)*a1)+((int)Math.pow(10d, (double)m)*a3)+a2;
		}
	}
	
	/**
	 * Splits a given integer in half by m.
	 * @param int	number	
	 * @param int 	m		Power exponent.
	 * @return int[]
	 */
	private int[] splitInTwo(int number, int m) {
		String nStr = String.valueOf(number);
		int[] split = new int[2];
		
		if(nStr.length() <= m) {
			// number shorter or equal to half(m)
			split[0] = 0;
			split[1] = number;
		} else {
			// number longer than half(m)
			split[0] = Integer.parseInt(nStr.substring(0, nStr.length()-m ));
			split[1] = Integer.parseInt(nStr.substring(nStr.length()-m));
		}
		return split;
	}
}
