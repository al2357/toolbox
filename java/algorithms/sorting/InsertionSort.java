package sorting;

import java.util.ArrayList;

public class InsertionSort extends Sort {
	
	InsertionSort(ArrayList<Integer> ul) {
		this.ul = ul;
	}
	
	public void run() { this.insertionSort(); }
	
	/**
	 * Insertion sort.
	 * Scan up then compare down with already sorted entries and optionally replace.
	 */
	public void insertionSort() {
		for(int i = 1; i < ul.size(); i++) {
			for(int j = i; j > 0; j--) {
				if(this.ul.get(j) < this.ul.get(j-1)) {
					this.swap(j, j-1);
				} else {
					break;
				}
			}
		}
	}
}
