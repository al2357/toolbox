package sorting;

import java.util.ArrayList;

public class SelectionSort extends Sort {

	SelectionSort(ArrayList<Integer> ul) {
		this.ul = ul;
	}
	
	public void run() { this.selectionSort(); }
	
	/**
	 * Selection sort.
	 * Go 1-by-1 then scan up and compare, then replace.
	 */
	public void selectionSort() {
		int minIdx;
		for(int i = 0; i < this.ul.size(); i++) {
			minIdx = i;
			for(int j = i; j < this.ul.size(); j++) {
				if(this.ul.get(minIdx) > this.ul.get(j)) { minIdx = j; }
			}
			if(i != minIdx) { this.swap(minIdx, i); }
		}
	}
}
