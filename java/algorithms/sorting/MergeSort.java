package sorting;

import java.util.ArrayList;
import java.util.List;

public class MergeSort extends Sort {
	
	/** Merge Sort inversions count */
	private long mergeSortSI = 0;
	
	/**
	 * Merge sort.
	 * Splits array in 2 recursively. Then merges and sorts. Worst case: n * log n
	 */
	public MergeSort(List<Integer> uL) {
		this.mergeSortSI = 0;
		this.ul = uL;
	}
	
	/**
	 * Initiates merge sort
	 */
	public void run() {
		this.ul = mergeSortR(this.ul);
	}
	
	/**
	 * Merge sort - recurring method
	 * @param list
	 * @return
	 */
	private List<Integer> mergeSortR (List<Integer> list) {
		if(list.size() > 1) {
			int half = list.size() / 2;
			List<Integer> sortedHalf1 = this.mergeSortR(list.subList(0, half));
			List<Integer> sortedHalf2 = this.mergeSortR(list.subList(half, list.size()));
			List<Integer> mergedList = new ArrayList<Integer>();
			int l1i = 0;
			int l2i = 0;
			
			// Merge the two
			while(l1i < half && l2i < sortedHalf2.size()) {
				if(sortedHalf1.get(l1i) < sortedHalf2.get(l2i)) {
					mergedList.add(sortedHalf1.get(l1i));
					l1i++;
				} else {
					// Merge from li2, here we increase split-inversion counter
					this.mergeSortSI = this.mergeSortSI + (sortedHalf1.size() - l1i);
					mergedList.add(sortedHalf2.get(l2i));
					l2i++;
				}
			}
			
			// Copy the remainder
			if(l1i < half) {
				for(int i = l1i; i < half; i++) {
					mergedList.add(sortedHalf1.get(i));
				}
			} else if (l2i < sortedHalf2.size()) {
				for(int i = l2i; i < sortedHalf2.size(); i++) {
					mergedList.add(sortedHalf2.get(i));
				}
			}
			return mergedList;
		} else {
			return list;
		}
	}
}