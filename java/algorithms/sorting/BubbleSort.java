package sorting;

import java.util.ArrayList;

public class BubbleSort extends Sort {

	BubbleSort(ArrayList<Integer> ul) {
		this.ul = ul;
	}
	
	public void run() { this.bubbleSort(); }
	
	/**
	 * Bubble sort.
	 * Iterate n times and swap pairs if necessary.
	 */
	public void bubbleSort() {
		for(int i = 0; i < this.ul.size(); i++) {
			for(int j = 0; j < this.ul.size()-1; j++) {
				if(this.ul.get(j) > this.ul.get(j + 1)) { this.swap(j, j+1);; }
			}
		}
	}
}
