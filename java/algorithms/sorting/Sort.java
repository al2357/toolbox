package sorting;

import java.util.List;

public abstract class Sort {
	
	/** Unsorted list */
	public List<Integer> ul;
	
	/**
	 * Swaps to elements of an array.
	 * @param i1
	 * @param i2
	 */
	protected void swap(int i1, int i2) {
		int temp = this.ul.get(i1);
		this.ul.set(i1, this.ul.get(i2));
		this.ul.set(i2, temp);
	}
	
	/**
	 * Prints unsorted list to the console.
	 */
	public void printList() {
		for(int i = 0; i < ul.size(); i++) {
			System.out.print(this.ul.get(i)+", ");
		}
		System.out.println("");
	}
	
	public abstract void run();
}
