

public class Alg {

	public static void main(String[] args) {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		System.out.println(palindrome(alphabet));
		
		System.out.println(palindrome("mars"));
	}

	private static String palindrome(String str) {
		String enc = "";
		
		for(int i = 1; i<=str.length(); i++) {
			enc = enc + str.substring(str.length()-i, str.length()-i+1);
		}
		return enc;
	}
	
}
