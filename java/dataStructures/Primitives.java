package net.jvt;

public class Primitives {
	public void testPrimitives() {
		float weight_f = 4.23f;
		
		// Implicit casting
		double weight = 5.4;
		System.out.println(weight);
		
		// Explicit casting
		int weight_int = (int) weight;
		int weight_int2 = (int) weight_f;
		System.out.print(weight_int+", ");
		System.out.print(weight_int2);
	}
	
	public void passByReference() {
		
	}
}
