package net.jvt;

import java.util.ArrayList;
import java.util.List;

public class CollectionsLists {

	public void testArrayList() {
		// Array list: no need to specify size, size is changeable
		// no primitive datatypes inside arrayList
		// arraylist more flexible and useful than array
		List<Double> arrayList = new ArrayList<Double>();
		arrayList.add(4.323);
		arrayList.add(7.45);
		arrayList.add(2.919);
		arrayList.add(7.45);
		arrayList.remove(7.45);
		for(Double d : arrayList) {
			System.out.println(Double.toString(d));
		}
	}
	
	public void testLinkedList() {
		
	}
	
}
