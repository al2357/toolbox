package net.jvt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DataStructures {
	public void testDataStructures() {
		//this.testArray();
		//this.testArrayList();
		this.testMap();
	}
	
	private void testArray() {
		// Array: need to specify size, size is not changeable
		int[] intArr = new int[5];
		intArr[2] = 4;
		System.out.println(Arrays.toString(intArr));
	}
	
	private void testMap() {
		// Map: holds key value pairs
		Map mapA = new HashMap();
		Map mapB = new TreeMap();
		
		// Hash Map
		mapA.put("name1", "john");
		mapA.put("name2", "mike");
		mapA.put("name3", 43534);
		
		Object t = mapA.get("name3");
		
		Iterator keyIterator = mapA.keySet().iterator();
		while(keyIterator.hasNext()) {
			Object key = keyIterator.next();
			Object value = mapA.get(key);
			System.out.println(key + ": " + value);
		}
		
		// Tree Map - ordered
		mapB.put("name5", "Anna");
		mapB.put("name6", "Anna");
		mapB.put("name7", "Gay");
		System.out.println(mapB.size());
		
		Iterator keyIteratorB = mapB.keySet().iterator();
		while(keyIteratorB.hasNext()) {
			Object keyB = keyIteratorB.next();
			Object valueB = mapB.get(keyB);
			System.out.println(keyB + ": " + valueB);
		}
		
	}
}
