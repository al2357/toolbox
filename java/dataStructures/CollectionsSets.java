package net.jvt;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class CollectionsSets {

	public void testSets() {
		// backed by HashMap, no guarante of order
		Set hashSet = new HashSet();
		//Set enumSet = new EnumSet();
		
		// order of insertion is maintained
		Set linkedSet = new LinkedHashSet();
		
		// sorting order is maintained
		Set treeSet = new TreeSet();
		
		// Hash Set only for Floats
		Set<Float> floatSet = new HashSet<Float>();
		
		for(int i = 100; i>0; i = i-10) {
			hashSet.add(i);
			linkedSet.add(i);
			treeSet.add(i);
			
			Float nf = (float) i;
			floatSet.add(nf);
		}
		
		// try to put same value twice
		hashSet.add("test hs");
		linkedSet.add("test ls");
		//treeSet.add("test ts");
		
		System.out.println("Hash set size: " + hashSet.size());
		
		// Loop through set with for loop
		for(Object val : treeSet) {
			System.out.print(val+", ");
		}
		System.out.println("");
		Iterator it = hashSet.iterator();
		while(it.hasNext()) {
			Object val = it.next();
			System.out.print(val+", ");
		}
		
		// Display sets
		/*
		System.out.println(hashSet);
		System.out.println(linkedSet);
		System.out.println(treeSet);
		System.out.println(floatSet);
		*/
	}
	
}
